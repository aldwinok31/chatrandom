package com.ttoonic.chatguess.Activities;

import com.ttoonic.chatguess.BaseActivity;
import com.ttoonic.chatguess.Interactive.FragmentInteractive;

public abstract class TabBaseHolderActivity extends BaseActivity implements FragmentInteractive {

    @Override
    protected void on_base_signal(Object object) {

    }

    @Override
    public Object onFragmentSignal(Object data) {
        return null;
    }

    @Override
    public void onChangeFragment() {

    }

    @Override
    public void onFragmentDirectCall(Object object) {

    }
}
