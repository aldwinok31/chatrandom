package com.ttoonic.chatguess.Activities;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.emoji.bundled.BundledEmojiCompatConfig;
import androidx.emoji.text.EmojiCompat;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.ttoonic.chatguess.Interactive.ActivityInteractive;
import com.ttoonic.chatguess.Model.User;
import com.ttoonic.chatguess.R;
import com.ttoonic.chatguess.ViewHandlers.PagerAdapter;

import java.util.UUID;


public class FragmentHolder extends TabBaseHolderActivity implements ViewPager.OnPageChangeListener
        , TabLayout.OnTabSelectedListener {
    private User user;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_holder);
        this.tabLayout = findViewById(R.id.tab_layout);
        this.tabLayout.addTab(tabLayout.newTab().setText("Option"));
        this.tabLayout.addTab(tabLayout.newTab().setText("Message"));
        this.tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        this.viewPager = findViewById(R.id.pager);
        final PagerAdapter pagerAdapter = new PagerAdapter(getSupportFragmentManager(), 0, tabLayout.getTabCount()
                , this);
        this.viewPager.setAdapter(pagerAdapter);
        this.viewPager.addOnPageChangeListener(this);
        this.tabLayout.addOnTabSelectedListener(this);
        EmojiCompat.Config config = new BundledEmojiCompatConfig(this);
        EmojiCompat.init(config);

        this.user = new User();
        this.user.setId(shared_preferences());
        this.user.setName("Duckling");

    }

    private String shared_preferences(){
        SharedPreferences sharedPreferences = getSharedPreferences("chat_random",MODE_PRIVATE);
        if(sharedPreferences.contains("user_id")){
            String uniqueID = UUID.randomUUID().toString();
            return sharedPreferences.getString("user_id",uniqueID);
        }
        else{
            String uniqueID = UUID.randomUUID().toString();
            SharedPreferences.Editor editor = getSharedPreferences("chat_random", MODE_PRIVATE).edit();
            editor.putString("user_id", uniqueID);
            editor.apply();
            return uniqueID;
        }

    }


    @Override
    protected void on_base_signal(Object object) {
        super.on_base_signal(object);
    }

    @Override
    public Object onFragmentSignal(Object data) {
        if(data instanceof ActivityInteractive){
            ((ActivityInteractive) data).onActivityCallback(this.user);
        }

        return super.onFragmentSignal(data);
    }

    @Override
    public void onChangeFragment() {
        super.onChangeFragment();
    }

    @Override
    public void onFragmentDirectCall(Object object) {
        super.onFragmentDirectCall(object);
        if(object instanceof Integer){
            if ((Integer)object != 0) {
                this.tabLayout.getTabAt(1).setText("Messages" + "(" + String.valueOf((Integer) object) + ")");
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        tabLayout.setScrollPosition(position,positionOffset,true);

    }

    @Override
    public void onPageSelected(int position) {
        if(position == 1) {
            tabLayout.getTabAt(position).setText("Messages");
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        this.viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        this.viewPager.setCurrentItem(tab.getPosition());

    }

    @Override
    public void onBackPressed() {

    }
}
