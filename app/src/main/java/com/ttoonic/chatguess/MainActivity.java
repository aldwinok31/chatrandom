package com.ttoonic.chatguess;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.ttoonic.chatguess.Activities.FragmentHolder;

public class MainActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        preload_timer();

    }

    @Override
    protected void on_base_signal(Object object) {
        Intent intent = new Intent(getApplicationContext(), FragmentHolder.class);
        startActivity(intent);
    }


}
