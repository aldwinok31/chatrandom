package com.ttoonic.chatguess.Interactive;

public interface FragmentInteractive<T>  {

    public T onFragmentSignal(T data);
    public void onChangeFragment();
    public void onFragmentDirectCall(Object object);

}
