package com.ttoonic.chatguess.Interactive;

public interface ActivityInteractive<T> {
    public void onActivityCallback(T data);
}
