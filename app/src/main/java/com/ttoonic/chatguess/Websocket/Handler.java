package com.ttoonic.chatguess.Websocket;

import android.util.Log;

import com.google.gson.Gson;
import com.ttoonic.chatguess.Model.Message;
import com.ttoonic.chatguess.Model.User;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

public class Handler extends WebSocketListener {
public final String TAG = " WEB SOCKET LISTENER ";
private SocketInteractive interactive;
private Object object;
    @Override
    public void onOpen(@NotNull WebSocket webSocket, @NotNull Response response) {
        super.onOpen(webSocket, response);
        Log.d(TAG, "onOpen: " + response);
        this.interactive.onSocketStart(webSocket);
    }

    @Override
    public void onClosed(@NotNull WebSocket webSocket, int code, @NotNull String reason) {
        super.onClosed(webSocket, code, reason);
        Log.d(TAG, "onClosed: ");
    }

    @Override
    public void onClosing(@NotNull WebSocket webSocket, int code, @NotNull String reason) {
        super.onClosing(webSocket, code, reason);
        Log.d(TAG, "onClosing: ");
        webSocket.close(1000,"Closing");
    }

    @Override
    public void onFailure(@NotNull WebSocket webSocket, @NotNull Throwable t, @Nullable Response response) {
        super.onFailure(webSocket, t, response);
        this.interactive.onSocketEnd(response);
    }

    @Override
    public void onMessage(@NotNull WebSocket webSocket, @NotNull String text) {
        super.onMessage(webSocket, text);
        Gson gson = new Gson();
        Message message = gson.fromJson(text,Message.class);
        Log.d(TAG, "onMessage:1 " + message.getMessage_type());
        if(message.getMessage_type().equals("JOINED")){
            this.interactive.onSocketStart(message);
        }
        else if(message.getMessage_type().equals("JOINED-MAIN")){
            this.interactive.onSocketStart(message);
        }
        else if (message.getMessage_type().equals("TYPING")){
            this.interactive.onSocketPinging(message);
        }
        else {
            this.interactive.onSocketEvent(message);
        }
        }

    @Override
    public void onMessage(@NotNull WebSocket webSocket, @NotNull ByteString bytes) {
        super.onMessage(webSocket, bytes);
        this.interactive.onSocketEvent(bytes);
    }

    public Handler(SocketInteractive webSocketListener,Object object){
        this.interactive = webSocketListener;
        this.object = object;
    }

    public interface SocketInteractive<T> {
        void onSocketStart(T object);
        void onSocketEvent(T object);
        void onSocketEnd(T object);
        void onSocketPinging(T object);
    }

}
