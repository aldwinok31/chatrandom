package com.ttoonic.chatguess.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ttoonic.chatguess.Interactive.FragmentInteractive;
import com.ttoonic.chatguess.R;

public class InitializeFragment extends BaseFragment {
    private View view;
    public InitializeFragment(FragmentInteractive fragmentInteractive) {
        super(fragmentInteractive);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.fragmentInteractive.onFragmentSignal(this);
        return this.view = inflater.inflate(R.layout.fragment_initialize,null);
    }



    @Override
    public void onActivityCallback(Object data) {
        super.onActivityCallback(data);
    }
}
