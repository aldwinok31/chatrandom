package com.ttoonic.chatguess.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.ttoonic.chatguess.Interactive.ActivityInteractive;
import com.ttoonic.chatguess.Interactive.FragmentInteractive;
import com.ttoonic.chatguess.R;

public abstract class BaseFragment extends Fragment implements ActivityInteractive {
    private View view;
    protected FragmentInteractive fragmentInteractive;

    public BaseFragment(FragmentInteractive fragmentInteractive) {
        this.fragmentInteractive = fragmentInteractive;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCallback(Object data) {

    }
}
