package com.ttoonic.chatguess.Fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.emoji.widget.EmojiButton;
import androidx.emoji.widget.EmojiEditText;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.ttoonic.chatguess.Interactive.FragmentInteractive;
import com.ttoonic.chatguess.Model.Message;
import com.ttoonic.chatguess.Model.User;
import com.ttoonic.chatguess.R;
import com.ttoonic.chatguess.ViewHandlers.ViewAdapterHandler;
import com.ttoonic.chatguess.Websocket.Handler;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.WebSocket;
import okio.ByteString;

public class ChatFragment  extends  BaseFragment implements View.OnClickListener , Handler.SocketInteractive {
    private View view;
    private ImageButton imageButton;
    private OkHttpClient  httpClient;
    private User user;
    private ViewAdapterHandler viewAdapterHandler;
    private RecyclerView recyclerView;
    private ArrayList<Message> messages;
    private WebSocket webSocket;
    public ChatFragment(FragmentInteractive fragmentInteractive) {
        super(fragmentInteractive);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.fragmentInteractive.onFragmentSignal(this);
        return this.view = inflater.inflate(R.layout.fragment_chat,null);
    }

    @Override
    public void onStart() {
        super.onStart();
        Button button = this.view.findViewById(R.id.search_user_button);
        button.setOnClickListener(this);
        View view = this.view.findViewById(R.id.refresh);
        view.setOnClickListener(this);
        final EmojiEditText editText = this.view.findViewById(R.id.message_edit);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(webSocket != null && user != null){
                    Message message = new Message(editText.getText().toString(), new Date().toString()
                            , user.getId(), "TYPING");
                    Gson gson = new Gson();
                    String json = gson.toJson(message);
                    byte[] bytes = json.getBytes(StandardCharsets.UTF_8);
                    webSocket.send(new ByteString(bytes));
                }
            }
        });


    }

    private void initialize(String path){
        if(this.webSocket != null){
         this.webSocket.close(1000,"websocket closed");
         this.webSocket = null;
        }
        this.imageButton = this.view.findViewById(R.id.refresh);
        this.imageButton.setOnClickListener(this);
        this.httpClient = new OkHttpClient();
        this.messages = new ArrayList<>();
        this.recyclerView = this.view.findViewById(R.id.recycler_view);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        this.viewAdapterHandler = new ViewAdapterHandler(this.messages, getContext(), this.user);
        this.recyclerView.setAdapter(this.viewAdapterHandler);
        start_socket(path);
        EmojiButton button = this.view.findViewById(R.id.send_message);
        button.setOnClickListener(this);
    }

    private void search(){
        if(this.webSocket != null){
            this.webSocket.close(1000,"websocket closed");
            this.webSocket = null;
        }

        this.httpClient = new OkHttpClient();
        Request request = new Request.Builder().url("ws://192.168.254.104:8080/ws/chat/searching/").build();
        Handler handler = new Handler(this,this.user);
        webSocket = httpClient.newWebSocket(request,handler);
        httpClient.dispatcher().executorService().shutdown();
    }

    @Override
    public void onActivityCallback(Object data) {
        if(data instanceof User){
            this.user = (User) data;
        }
        super.onActivityCallback(data);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.refresh){
            if (this.socket_search_timer != null) {
                this.socket_search_timer.cancel();
            }
            re_init();
        }
        if(v.getId() == R.id.search_user_button){
            View view_layout = this.view.findViewById(R.id.search_user);
            ProgressBar progressBar = this.view.findViewById(R.id.search_progress);
            TextView textView = this.view.findViewById(R.id.search_text);

            Button button = this.view.findViewById(v.getId());
            button.setText("Searching...");

            progressBar.setVisibility(View.VISIBLE);


            if(this.webSocket == null) {
               search();
            }
        }
        if(v.getId() == R.id.send_message) {
            if(this.webSocket != null) {
                final EmojiEditText editText = this.view.findViewById(R.id.message_edit);

                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                if(!editText.getText().toString().equals("")) {
                    Message message = new Message(editText.getText().toString(), new Date().toString()
                            , this.user.getId(), "CHAT");
                    Gson gson = new Gson();
                    String json = gson.toJson(message);
                    byte[] bytes = json.getBytes(StandardCharsets.UTF_8);
                    webSocket.send(new ByteString(bytes));
                    editText.setText("");
                    editText.clearFocus();
                }
                else {
                    editText.setError("Enter Message!");
                }
            }
        }
    }
    public void start_socket(String path){
        Request request = new Request.Builder().url("ws://192.168.254.104:8080/ws/chat/"+path+"/").build();
        Handler handler = new Handler(this,this.user);
         webSocket = httpClient.newWebSocket(request,handler);
        httpClient.dispatcher().executorService().shutdown();
    }

    @Override
    public void onSocketStart(final Object object) {
    getActivity().runOnUiThread(new Runnable() {
        @Override
        public void run() {
            if(object instanceof Message){
                if(socket_search_timer != null){
                    socket_search_timer.cancel();
                    socket_search_timer = null;
                }
                  if(!((Message) object).getMessage_type().equals("JOINED_MAIN")) {
                      View view_layout = view.findViewById(R.id.search_user);
                      view_layout.setVisibility(View.GONE);
                      initialize(((Message) object).getContent());
                  }
            }
        }
    });
    }
    private CountDownTimer socket_search_timer;
    @Override
    public void onSocketEvent(final Object object) {
   if(object instanceof Message){
       if(!((Message) object).getMessage_type().equals("SEARCH") ) {
           if(socket_search_timer !=null) {
               socket_search_timer.cancel();
               socket_search_timer = null;
           }
           if (this.viewAdapterHandler != null) {
               if(((Message) object).getMessage_type().equalsIgnoreCase("disconnection")){
                   webSocket.close(1090, "CLOSE");
                   webSocket.cancel();
                   re_init();
              }
              else {
                  getActivity().runOnUiThread(new Runnable() {
                      @Override
                      public void run() {
                          TextView t = view.findViewById(R.id.typing_flag);
                          t.setVisibility(View.GONE);
                          viewAdapterHandler.addData(object);
                          viewAdapterHandler.notifyDataSetChanged();
                          fragmentInteractive.onFragmentDirectCall(messages.size());
                          recyclerView.scrollToPosition(viewAdapterHandler.getItemCount() - 1);
                          if(!((Message) object).getSender().equals(user.getId())) {
                              create_vibration();
                          }
                      }
                  });
              }
           }
       }
       else{

           if(socket_search_timer == null && ((Message) object).getMessage_type().equals("SEARCH")){
                   getActivity().runOnUiThread(new Runnable() {
                       @Override
                       public void run() {
                           socket_search_timer = new CountDownTimer(30000, 5000) {
                               @Override
                               public void onTick(long millisUntilFinished) {

                                   Message message = new Message("Searching", new Date().toString()
                                           , user.getId(), "SEARCH");
                                   Gson gson = new Gson();
                                   String json = gson.toJson(message);
                                   byte[] bytes = json.getBytes(StandardCharsets.UTF_8);
                                   webSocket.send(new ByteString(bytes));
                               }

                               @Override
                               public void onFinish() {
                                   re_init();
                               }
                           }.start();
                       }
                   });
               }
           }

       }
   }


    private void re_init(){
        ProgressBar progressBar = view.findViewById(R.id.search_progress);

        Button button = view.findViewById(R.id.search_user_button);
        button.setText("Search");

        progressBar.setVisibility(View.INVISIBLE);
        if(socket_search_timer != null){
            socket_search_timer.cancel();
        }
        socket_search_timer = null;
        if(this.webSocket != null) {
            this.webSocket.close(1000, "CLOSE");
            this.webSocket.cancel();
            webSocket = null;
        }


        View view_layout = view.findViewById(R.id.search_user);
        view_layout.setVisibility(View.VISIBLE);

    }

    private void create_vibration(){
        Vibrator v = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            v.vibrate(500);
        }
    }
    @Override
    public void onSocketEnd(Object object) {
        if (this.socket_search_timer != null) {
            this.socket_search_timer.cancel();
        }

        getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    re_init();
                }
            });
    }

    @Override
    public void onSocketPinging(Object object) {
        if(object instanceof Message){

            if (!((Message) object).getSender().equals(user.getId())){
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                      final TextView t = view.findViewById(R.id.typing_flag);
                        t.setVisibility(View.VISIBLE);

                        final CountDownTimer timer = new CountDownTimer(3000,1000) {
                            @Override
                            public void onTick(long millisUntilFinished) {

                            }

                            @Override
                            public void onFinish() {
                                t.setVisibility(View.GONE);
                            }
                        };
                        timer.start();

                    }
                });
            }

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(this.webSocket != null){
            if(webSocket != null) {
                Message message = new Message("Searching", new Date().toString()
                        , this.user.getId(), "DISCONNECT");
                Gson gson = new Gson();
                String json = gson.toJson(message);
                byte[] bytes = json.getBytes(StandardCharsets.UTF_8);
                webSocket.send(new ByteString(bytes));
            }
            this.webSocket.close(1000,"DESTROY");
        }
     }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(this.webSocket != null){
            this.webSocket.close(1000,"DESTROY");
        }
    }
}
