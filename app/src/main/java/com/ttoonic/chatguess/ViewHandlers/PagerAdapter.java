package com.ttoonic.chatguess.ViewHandlers;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.ttoonic.chatguess.Fragments.ChatFragment;
import com.ttoonic.chatguess.Fragments.InitializeFragment;
import com.ttoonic.chatguess.Interactive.FragmentInteractive;

public class PagerAdapter extends FragmentStatePagerAdapter{
private FragmentInteractive fragmentInteractive;
private int num;

    public PagerAdapter(FragmentManager fragmentManager, int behavior, int num ,
                        FragmentInteractive fragmentInteractive){
        super(fragmentManager,behavior);
        this.fragmentInteractive = fragmentInteractive;
        this.num = num;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        switch (position){
            case 0:
                return new InitializeFragment(this.fragmentInteractive);
            case 1:
                return  new ChatFragment(this.fragmentInteractive);
        }
        return null;
    }

    @Override
    public int getCount() {
        return num;
    }
}
