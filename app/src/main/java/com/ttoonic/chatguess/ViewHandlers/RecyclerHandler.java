package com.ttoonic.chatguess.ViewHandlers;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.emoji.widget.EmojiTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.ttoonic.chatguess.R;

public class RecyclerHandler extends RecyclerView.ViewHolder {

    EmojiTextView textView;
    EmojiTextView textView_rcv;
    View cardView;
    View cardView_rcv;

    public RecyclerHandler(@NonNull View itemView) {
        super(itemView);
        this.textView = itemView.findViewById(R.id.message_content);
        this.textView_rcv = itemView.findViewById(R.id.message_content_reciever);
        this.cardView = itemView.findViewById(R.id.cardView);
        this.cardView_rcv = itemView.findViewById(R.id.cardView_rcv);
    }
}
