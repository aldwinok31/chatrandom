package com.ttoonic.chatguess.ViewHandlers;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ttoonic.chatguess.Model.Message;
import com.ttoonic.chatguess.Model.User;
import com.ttoonic.chatguess.R;

import java.util.ArrayList;

public class ViewAdapterHandler<T> extends RecyclerView.Adapter<RecyclerHandler> {
    private ArrayList<Message> messages;
    private View view;
    private Context context;
    private User user;

    @NonNull
    @Override
    public RecyclerHandler onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
         this.view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_dom,parent,false);
        return new RecyclerHandler(this.view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerHandler holder, int position) {
        if(messages.get(position).getSender().equals( this.user.getId()) ){
            holder.textView.setText(messages.get(position).getContent());
            holder.cardView_rcv.setVisibility(View.INVISIBLE);
        }
        else{
            holder.textView_rcv.setText(messages.get(position).getContent());
            holder.cardView.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public ViewAdapterHandler(ArrayList<Message> messages, Context context, User user) {
        this.messages = messages;
        this.context= context;
        this.user = user;
    }
    public void addData(T message){
        if (message instanceof  Message){
            this.messages.add((Message) message);
        }
    }
    public ArrayList<Message> getData(){
        return this.messages;
    }
}
